import _ from "lodash";

export const Paginate = (items, currentPage, pageSize) => {
    const startIndex = (currentPage - 1) * pageSize;

    const page = _(items).slice(startIndex).take(pageSize).value();

    return page;
};
