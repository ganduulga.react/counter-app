import { Fragment } from "react";
import Home from "./pages/Home";
import About from "./pages/About";
import { Route, Switch, Redirect } from "react-router-dom";
import Products from "./pages/Products";
import NotFound from "./pages/NotFound";
import Navbar from "./components/Navbar";
import LoginForm from "./components/loginForm";
import MovieForm from "./components/movieForm";

function App() {
    const text = "Text";
    return (
        <Fragment>
            <Navbar />
            <Switch>
                <Route
                    path="/home"
                    exact
                    render={(props) => <Home {...props} text={text} />}
                />
                <Route
                    path="/about"
                    render={(props) => <About text={text} {...props} />}
                />
                <Route
                    path="/products"
                    render={(props) => <Products text={text} {...props} />}
                />
                <Route
                    path="/movie/:id"
                    render={(props) => <MovieForm {...props} />}
                />
                {/* <Route
                    path="/products/:month?/:day?"
                    render={(props) => <Products text={text} {...props} />}
                /> */}
                <Route path="/login" component={LoginForm} />
                <Route path="/notfound" component={NotFound} />
                <Redirect from="/" exact to="/home" />
                <Redirect to="/notfound" />
            </Switch>
        </Fragment>
    );
}
export default App;

//TODO react-router-dom -> Redirect
