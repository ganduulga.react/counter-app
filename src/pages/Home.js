import React from "react";
import Movies from "../components/movies";

function Home(props) {
    return (
        <div>
            <Movies router={props} />
        </div>
    );
}

export default Home;
