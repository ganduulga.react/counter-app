import React from "react";
import queryString from "query-string";

function About(props) {
    const search = queryString.parse(props.location.search);
    console.log(search.name);
    return (
        <div>
            <p onClick={() => props.history.replace("/")}>Go back</p>
        </div>
    );
}

export default About;
