import React, { Component } from "react";

export default class TableHead extends Component {
    render() {
        const { columns, onSort } = this.props;
        return (
            <thead>
                <tr>
                    {columns.map((col) => {
                        return (
                            <th
                                key={col.path || col.key}
                                onClick={() => onSort(col.path)}
                            >
                                {col.label}
                            </th>
                        );
                    })}
                </tr>
            </thead>
        );
    }
}
