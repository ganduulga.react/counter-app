import React from "react";

function ListGenre({ genres, onClick, selectedGenre, name, id }) {
    return (
        <ul className="list-group">
            {genres.map((genre) => (
                <li
                    key={genre.name}
                    className={
                        selectedGenre[id] === genre[id]
                            ? "list-group-item active"
                            : "list-group-item"
                    }
                    onClick={() => onClick(genre)}
                >
                    {genre[name]}
                </li>
            ))}
        </ul>
    );
}

ListGenre.defaultProps = {
    name: "name",
    id: "_id",
};

export default ListGenre;
