import React, { Component } from "react";
import { getMovies } from "../services/fakeMovieService";
import { getGenres } from "../services/fakeGenreService";
import { Paginate } from "../utils/paginate";
import ListGenre from "./listGenre";
import Pagination from "./pagination";
import MoviesTable from "./moviesTable";
import _ from "lodash";

class Movies extends Component {
    state = {
        movies: [],
        genres: [],
        pageSize: 4,
        currentPage: 1,
        selectedGenre: {},
        sortColumn: {
            path: "title",
            order: "asc",
        },
    };

    componentDidMount() {
        const genres = [{ name: "All genres" }, ...getGenres()];

        this.setState({ movies: getMovies(), genres });
    }

    handleDelete = (id) => {
        const movies = this.state.movies.filter((m) => m._id !== id);
        this.setState({ movies });
    };

    handleLike = (movie) => {
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index].liked = !movies[index].liked;

        this.setState({ movies });
    };

    handlePageChange = (page) => {
        this.setState({ currentPage: page });
    };

    handleSelectGenre = (genre) => {
        this.setState({ selectedGenre: genre, currentPage: 1 });
    };

    handleSort = (sortColumn) => {
        this.setState({ sortColumn });
    };

    onEdit = (id) => {
        const { router } = this.props;
        router.history.push(`/movie/${id}`);
    };

    render() {
        const { length: count } = this.state.movies;
        const {
            movies: allMovies,
            currentPage,
            pageSize,
            genres,
            selectedGenre,
            sortColumn,
        } = this.state;

        if (count === 0) return <p>Database in empty</p>;

        let filteredData;

        if (selectedGenre && selectedGenre._id) {
            filteredData = allMovies.filter(
                (m) => m.genre.name === selectedGenre.name
            );
        } else {
            filteredData = allMovies;
        }

        const sortedData = _.orderBy(
            filteredData,
            [sortColumn.path],
            [sortColumn.order]
        );

        const movies = Paginate(sortedData, currentPage, pageSize);

        return (
            <div className="row m-2">
                <div className="col-3">
                    <ListGenre
                        genres={genres}
                        selectedGenre={selectedGenre}
                        onClick={this.handleSelectGenre}
                    />
                </div>
                <div className="col">
                    <p>Database in {filteredData.length} items.</p>
                    <MoviesTable
                        movies={movies}
                        onLike={this.handleLike}
                        onDelete={this.handleDelete}
                        onSort={this.handleSort}
                        sortColumn={sortColumn}
                        onEdit={this.onEdit}
                    />
                    <Pagination
                        itemsCount={filteredData.length}
                        pageSize={this.state.pageSize}
                        currentPage={this.state.currentPage}
                        onClick={this.handlePageChange}
                    />
                </div>
            </div>
        );
    }
}

export default Movies;
