import React, { Component } from "react";
import _ from "lodash";
import PropTypes from "prop-types";

export default class Pagination extends Component {
    render() {
        const { itemsCount, pageSize, onClick, currentPage } = this.props;
        const pages = _.range(1, Math.ceil(itemsCount / pageSize) + 1);

        return (
            <nav>
                <ul className="pagination">
                    {pages.map((page) => (
                        <li
                            key={page}
                            className={
                                currentPage === page
                                    ? "page-item active"
                                    : "page-item"
                            }
                        >
                            <a
                                className="page-link"
                                onClick={() => onClick(page)}
                            >
                                {page}
                            </a>
                        </li>
                    ))}
                </ul>
            </nav>
        );
    }
}

Pagination.propTypes = {
    itemsCount: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired,
    currentPage: PropTypes.number.isRequired,
};
