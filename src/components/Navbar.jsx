import React from "react";
import { Link, NavLink } from "react-router-dom";

function Navbar() {
    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/">
                    Navbar
                </Link>
                <div
                    className="collapse navbar-collapse"
                    id="navbarNavAltMarkup"
                >
                    <div className="navbar-nav">
                        <NavLink
                            className="nav-item nav-link"
                            to="/home"
                            activeClassName="nav-item nav-link active"
                        >
                            Home
                        </NavLink>
                        <NavLink
                            className="nav-item nav-link"
                            activeClassName="nav-item nav-link active"
                            to="/about"
                        >
                            About
                        </NavLink>
                        <NavLink
                            className="nav-item nav-link"
                            activeClassName="nav-item nav-link active"
                            to="/products"
                        >
                            Product
                        </NavLink>
                        <NavLink
                            className="nav-item nav-link"
                            activeClassName="nav-item nav-link active"
                            to="/login"
                        >
                            Login
                        </NavLink>
                    </div>
                </div>
            </nav>
        </React.Fragment>
    );
}

export default Navbar;
