import React, { Component } from "react";
import Like from "./like";
import TableHead from "./tableHead";

export default class MoviesTable extends Component {
    columns = [
        {
            path: "title",
            label: "Title",
        },
        {
            path: "genre.name",
            label: "Genre",
        },
        {
            path: "numberInStock",
            label: "Stock",
        },
        {
            path: "dailyRentalRate",
            label: "Rate",
        },
        {
            key: "like",
            content: (movie) => (
                <Like
                    onClick={this.props.onLike}
                    isLike={movie.liked}
                    movie={movie}
                />
            ),
        },
        { key: "delete" },
    ];
    raiseSort = (path) => {
        const sortColumn = { ...this.props.sortColumn };

        if (sortColumn.path === path) {
            sortColumn.order = sortColumn.order === "asc" ? "desc" : "asc";
        } else {
            sortColumn.path = path;
            sortColumn.order = "asc";
        }

        this.props.onSort(sortColumn);
    };
    render() {
        const { movies, onLike, onDelete, onEdit } = this.props;
        return (
            <table className="table m-2">
                <TableHead columns={this.columns} onSort={this.raiseSort} />
                <tbody>
                    {movies.map((movie) => {
                        return (
                            <tr key={movie._id}>
                                <th>{movie.title}</th>
                                <th>{movie.genre.name}</th>
                                <th>{movie.numberInStock}</th>
                                <th>{movie.dailyRentalRate}</th>
                                <th>
                                    <Like
                                        onClick={onLike}
                                        isLike={movie.liked}
                                        movie={movie}
                                    />
                                </th>
                                <th>
                                    <button
                                        onClick={() => onDelete(movie._id)}
                                        className="btn btn-danger btn-sm mr-2"
                                    >
                                        Delete
                                    </button>
                                    <button
                                        onClick={() => onEdit(movie._id)}
                                        className="btn btn-danger btn-sm"
                                    >
                                        Edit
                                    </button>
                                </th>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }
}
