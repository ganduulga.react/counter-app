import React, { Component } from "react";

export default class Like extends Component {
    render() {
        const classes = `fa fa-heart${this.props.isLike ? "-o" : ""}`;

        return (
            <i
                onClick={() => this.props.onClick(this.props.movie)}
                className={classes}
            ></i>
        );
    }
}
