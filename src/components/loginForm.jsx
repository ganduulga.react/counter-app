import React, { Component } from "react";
import Joi from "joi-browser";

export default class LoginForm extends Component {
    state = {
        account: {
            username: "",
            password: "",
        },
        error: {},
        disabled: true,
    };

    schema = {
        username: Joi.string().min(2).required(),
        password: Joi.string().min(8).required(),
    };

    validate = () => {
        const error = {};

        const result = Joi.validate(this.state.account, this.schema, {
            abortEarly: false,
        });

        if (!result.error) {
            return error;
        } else {
            for (let item of result.error.details) {
                error[item.path[0]] = item.message;
            }

            return error;
        }
    };

    handleChange = (event) => {
        this.setState({ disabled: false });
        const account = { ...this.state.account };
        account[event.target.id] = event.target.value;

        this.setState({ account });
    };

    handleSubmit = () => {
        const error = this.validate();

        this.setState({ error });
    };

    render() {
        const { error, disabled } = this.state;
        return (
            <React.Fragment>
                <main className="container mt-5">
                    {/* <form> */}
                    <h2>Login</h2>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input
                            type="text"
                            className="form-control"
                            id="username"
                            onChange={this.handleChange}
                        />
                        {error.username && (
                            <small
                                id="username"
                                className="form-text text-muted"
                            >
                                <p className="text-danger">{error.username}</p>
                            </small>
                        )}
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            id="password"
                            onChange={this.handleChange}
                        />
                        {error.password && (
                            <small
                                id="username"
                                className="form-text text-muted"
                            >
                                <p className="text-danger">{error.password}</p>
                            </small>
                        )}
                    </div>
                    <button
                        disabled={disabled}
                        type="submit"
                        onClick={this.handleSubmit}
                        className="btn btn-primary"
                    >
                        Submit
                    </button>
                </main>
            </React.Fragment>
        );
    }
}
